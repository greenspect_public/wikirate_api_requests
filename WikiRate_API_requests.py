# -*- coding: utf-8 -*-
__version__ = "0.1"

import os
import re
import sys
import traceback
from io import StringIO
from pathlib import Path

import pandas as pd  # type: ignore
import requests
from loguru import logger
from requests.exceptions import ConnectionError, HTTPError, RequestException, Timeout


class RetriesException(Exception):
    pass


class WikiRateDownloader:
    def __init__(
        self,
        metric_creator,
        metric_name,
        save_to_file=True,
        filedir="data/",
        filename_suffix=None,
        format="csv",
        **_kwargs,
    ):
        # --------------------------------------------------
        # Section to config the {metric}+Answer API request:
        # --------------------------------------------------
        # 'metric_creator', and 'metric_name' define the request string. With
        # this scheme a list of companies having an record or answer to this metric on
        # WikiRate are returned.
        # In the 'query' dict more filters can be applied to the search.'limit' sets
        # the limit of search results. Note that 'format': 'json' is needed to return
        # the correct format.
        self._server = "https://wikirate.org/"
        self.metric_creator = metric_creator
        self.metric_name = metric_name
        self.save_to_file = save_to_file
        self.filepath = filedir
        self.filename_suffix = filename_suffix

        secrets_file = os.path.join(
            os.path.join(os.path.dirname(__file__), ".secrets"), "wikirate_api_key"
        )

        if os.path.exists(secrets_file):
            with open(secrets_file, "r") as f:
                self.secret = f.readline().strip()
        else:
            self.secret = None

        # Save parameters for query
        self.query_pars = {
            "format": format,
        }
        for kwarg in _kwargs:
            if kwarg in [
                "status",
                "year",
                "value",
                "updated",
                "source",
                "project",
                "verification",
                "company_name",
                "company_id",
                "outliers",
            ]:
                self.query_pars[f"filter[{kwarg}]"] = _kwargs[kwarg]
            elif kwarg in ["limit"]:
                self.query_pars[kwarg] = _kwargs[kwarg]
            else:
                raise Exception(f'Error: Couldn\'t find filter parameter "{kwarg}".')

        self.df = self.download_data()

    def __repr__(self):
        return (
            "---------------------------\n"
            + "WikiRate API Request Object\n"
            + "---------------------------\n"
            + f"Metric Creator: {self.metric_creator}\n"
            + f"Metric Name: {self.metric_name}\n"
            + str(self.query_pars)
        )

    def _get(self):
        try:
            # Make GET request on WikiRate server:
            # the requests package converts the string to correct html format for
            # the request, 'timeout' sets optional parameter for timing for
            # triggering timeout exception.
            if self.secret is not None:
                self.query_pars["api_key"] = self.secret
            response = requests.get(
                f"{self._server}{self.metric_creator}+{self.metric_name}+Answer",
                params=self.query_pars,
                timeout=20,
            )
            logger.debug(f"Made request with this URL: {response.url}")

            response.raise_for_status()  # Raises HTTPError, if one occurred

        # Below is a comprehensive (but not complete) set of exceptions that can
        # handle most common errors during the request:
        except HTTPError as err_h:
            logger.error(f"HTTP error occurred: {err_h}")
            raise err_h
        except ConnectionError as err_c:
            logger.error(f"Connection error occurred: {err_c}")
            raise err_c
        except Timeout as err_t:
            logger.error(f"Timeout error occurred: {err_t}")
            raise err_t
        except RequestException as err_r:
            logger.error(f"Request error occurred: {err_r}")
            raise err_r
        else:
            logger.debug("Request was successful!")
            return response

    def download_data(self):
        # get raw response from request
        max_retries = 5
        retries = max_retries
        while retries > 0:
            try:
                response = self._get()
                break
            except KeyboardInterrupt:
                logger.error("Caught KeyboardInterrupt, terminating.")
                try:
                    sys.exit(0)
                except SystemExit:
                    os._exit(0)
            except:
                retries -= 1

        if not retries:
            logger.error(f"Max number of retries ({max_retries}) exceeded.")
            raise RetriesException("Too many retries")

        if self.save_to_file:
            Path(self.filepath).mkdir(exist_ok=True)

            def __format(string):
                return "_".join(re.findall(r"\w+", string))

            filepath = (
                self.filepath
                + __format(self.metric_creator)
                + "+"
                + __format(self.metric_name)
                + (self.filename_suffix if self.filename_suffix else "")
                + "."
                + self.query_pars["format"]
            )
            logger.debug("Saving request to " + filepath)

            with open(filepath, "wb") as _file:
                _file.write(response.content)

        if self.query_pars["format"] == "json":
            # Convert response json to pandas dataframe. Company list and data is found
            # in 'items' of request body.
            return pd.DataFrame(response.json()["items"])
        elif self.query_pars["format"] == "csv":
            # Convert response csv to pandas dataframe. User StringIO to convert str to
            # file object
            return pd.read_csv(StringIO(response.content.decode("utf-8")))

    def get_df(self):
        if self.df is None:
            self.df = self.download_data()
        return self.df


if __name__ == "__main__":
    datapath = "data/"

    for metric_name in [
        "Direct greenhouse gas (GHG) emissions (Scope 1) (G4-EN15-a)",
        # "Indirect greenhouse gas (GHG) emissions (Scope 2) (G4-EN16-a)",
        # "Indirect greenhouse gas (GHG) emissions (Scope 3), GRI 305-3"
        # " (formerly G4-EN17-a)"
    ]:
        scope_1 = WikiRateDownloader(
            "Global Reporting Initiative",
            metric_name,
            filedir=datapath,
            year="2017",
            status="known",
            save_to_file=True,
            limit=1200,
        )
        data = scope_1.get_df()

    # Get overview of data:
    print(data.info())
    print(data[["Company", "Year", "Value"]].head())
